$(function () {
    var userName;
    var userLastName;
    $('#editor').hide();
    $('#editorSub').hide();
    $('#edit').on ('click', function (e) {
        e.preventDefault();
        $('#editor').show();
    });
    var mail = 'bugaiyoff@gmail.com';
    var url  =  'http://146.185.154.90:8000/blog/' + mail;
    async function start() {
        var profile = await $.get(url + '/profile');
        $('#name').text(profile.firstName + ' ' + profile.lastName);
        userName = profile.firstName;
        userLastName = profile.lastName;
    }
    async function getProfile() {
        await $.get(url + '/profile');
        postProfile(userName,userLastName);
        $('#name').text(userName + ' ' + userLastName);
    }
    var postProfile = function(name, lastName) {
        if(!name || !lastName){
            alert('You must add first name and last name before')
        }else {
            $.ajax({
                method: 'POST',
                url: url + '/profile',
                data: {
                    firstName: name,
                    lastName: lastName
                }
            });
            getPosts();
        }
    };
    var sendMessage = function () {
          var message = $('#message').val();
          if(!message){
              alert('Message must be entered')
          }else {
              $.ajax({
                  method: 'POST',
                  url: url + '/posts',
                  data: {
                      message: message
                  }
              });
              getPosts();
              $('#message').val('');
          }
    };
    var getPosts = async function () {
        var posts = await $.get(url + '/posts');
        posts.map(function (p) {
            $('#list').prepend(`<li>
			<span class="autor">` + p.user.email + `</span>
			<span class="autor">` + p.user.firstName + `</span>
			<span class="autor">` + p.user.lastName + `</span>
			<p class="text">`+ p.message +`</p>
			<span class="date">`+ new Date(p.datetime) +`</span>
		</li>`);
        });
    };
    $('#save').on('click', function (e) {
        e.preventDefault();
        userName = $('#username').val();
        userLastName = $('#userlast').val();
        $('#name').html(userName + ' ' + userLastName);
        $('#editor').hide();
        getProfile();
    });
    $('#send').on('click', function () {
        if(!userName || !userLastName) {
            alert('You must add first name and last name before');
        }else {
            sendMessage();
        }
    });
    var subcribe = function () {
        var subscribe = $('#subs').val();
        if(!subscribe){
            alert('You must add email before');
        }else {
            $.ajax({
                method: 'POST',
                url: url +'/subscribe',
                data: {
                    email: subscribe
                }
            });
        }
        $('#subs').val('');
    };
    $('#subscribe').on('click', function (e) {
        e.preventDefault();
        $('#editorSub').show();
    });
    $('#add').on('click', function (e) {
        e.preventDefault();
        $('#editorSub').hide();
        subcribe();
    });
    async function print() {
        var posts = await $.get(url + '/posts');
        posts.map(function (p) {
            $('#list').prepend(`<li>
			<span class="autor">` + p.user.email + `</span>
			<span class="autor">` + p.user.firstName + `</span>
			<span class="autor">` + p.user.lastName + `</span>
			<p class="text">`+ p.message +`</p>
			<span class="date">`+ new Date(p.datetime) +`</span>
		</li>`);
        });
        console.log(posts);
        var time = posts[posts.length - 1].datetime;
        console.log(time);
        setInterval(async function () {
            var lastMessage = await $.get(url +'/posts?datetime' + time);

            if(lastMessage.length !== 0){
                console.log(lastMessage);
                lastMessage.map(function (m) {
                    $('#list').prepend(`<li>
                    <span class="autor">` + m.user.email + `</span>
                    <span class="autor">` + m.user.firstName + `</span>
			        <span class="autor">` + m.user.lastName + `</span>
                    <p class="text">`+ m.message +`</p>
                    <span class="date">`+ new Date(m.datetime) +`</span>
                </li>`);
                    time = m.datetime;
                });
            }
        },5000);
    }
    start();
    print();
});